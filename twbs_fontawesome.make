api = 2
core = 7.x

; Libraries
libraries[font-awesome][directory_name] = font-awesome
libraries[font-awesome][download][type] = file
libraries[font-awesome][download][url] = http://fontawesome.io/assets/font-awesome-4.6.3.zip
libraries[font-awesome][type] = library
